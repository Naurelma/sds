
const express = require("express")
const bodyParser = require("body-parser")
const path = require("path")


const port = 3000
var app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.use(express.static(path.join(__dirname, "public")))

app.get("/", (req, res)=>{
	res.send("Hello")
})

app.listen(port,()=>{
	console.log("server started on port: "+ port)
})