
/*database setup */
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/timeline', {useNewUrlParser: true});
var db = mongoose.connection;

/*Database */


/* schemas */
var eventSchema = new mongoose.Schema({
	name:{type:String, required:true},
	startDate: {type:Date, required:true},
	description: {type:String, default:""}
})



var eraSchema = new mongoose.Schema({
	name:{type:String, required:true},
	startDate: {type:Date, required:true},
	endDate: {type:Date,default:new Date()},
	description: {type:String, default:""}
})
/*
Date.now => new Date()
https://stackoverflow.com/questions/26475013/date-comparison-with-mongoose
*/
/*models / exports*/

var Event = module.exports.Event = mongoose.model("Event", eventSchema)
var Era = module.exports.Era = mongoose.model("Era", eraSchema)

/* Events */
module.exports.createEvent = function (uusi, cb) {
	
	var tmp = Event(uusi)
	tmp.save(cb)
}

module.exports.removeEvent = function (id, cb) {
	Event.deleteOne({_id:id}, cb)
}


module.exports.getEventsBetween = function (start, end, cb) {
	Event.find({}).
		where("startDate").gt(start).lt(end).
		sort("startDate").
		exec(cb)
}
module.exports.getEventByDate = function (start, cb) {
	/*
	return the closest date to the given date
	*/
	/*
	Didnt figure out simpler way to do this.
	*/
	Event.find({startDate: {$lte: start}}).sort({startDate: -1}).limit(1).exec((err1, result1) =>{
		Event.find({startDate: {$gte: start}}).sort({startDate: 1}).limit(1).exec((err2, result2) =>{
			/*they return lists, but i want them as simple values */
			
			result1 = result1[0]
			result2 = result2[0]
			if( err1 && err2){
				cb([err1,err2])
				return
			}
			/*
			console.log(result1)
			console.log(result2)
			console.log(Math.abs(start - result1.startDate))
			console.log(Math.abs(start - result2.startDate))
			*/

			if(!result1){
				cb(null, result2)

			} else if(!result2){
				cb(null, result1)

			} else if (
				Math.abs(start - result1.startDate) 
				<Math.abs(start - result2.startDate)){

				cb(null, result1)

			} else{
				cb(null, result2)

			}

		})
	})
	

}

/* Eras*/

module.exports.createEra = function (uusi, cb) {
	/*this disallows any non schema inputs */
	var tmp = Era(uusi)
	tmp.save(cb)
}

module.exports.removeEra = function (id, cb) {
	Era.deleteOne({_id:id}, cb)
}

module.exports.getErasContainedBetween = function (start, end, cb) {
	/*eras that start in the interval and end in the interval */
	Era.find({}).
		where("startDate").gte(start).lte(end).
		where("endDate").gte(start).lte(end).
		sort("startDate").
		exec(cb)
}


module.exports.getErasActiveBetween = function (start, end, cb) {
	/*eras that start before the interval's end and end after interval's start */
	Era.find({}).
		where("startDate").lte(end).
		where("endDate").gte(start).
		sort("startDate").
		exec(cb)
}




