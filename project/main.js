
const express = require("express")
const bodyParser = require("body-parser")
const path = require("path")

const models = require("./models")



/* App itself */
const port = 3000
var app = express()

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({extended: true}))


app.get("/", (req, res)=>{
	res.send(`Please use api/eras or api/events ...<br>
<br>
List of all routes: <br>
get("/api/events")<br>
post("/api/events")<br>
get("/api/events/byid/:_id")<br>
delete("/api/events/byid/:_id")<br>
get("/api/events/byinterval/:stdate/:enddate")<br>
get("/api/events/bydate/:stdate")<br>
<br>
get("/api/eras")<br>
post("/api/eras")<br>
get("/api/eras/byid/:_id")<br>
delete("/api/eras/byid/:_id")<br>
get("/api/eras/bycontained/:stdate/:enddate")<br>
get("/api/eras/byinterval/:stdate/:enddate")<br>
`)
	
})

function dbCallback(res) {
	/*it is allways the same thing */
	return (err, result) => {
		if (err){
			res.status(400)
			res.json(err)
		} else{
			res.json(result)
		}
	}
}

/* 
user inputs should be sanitized before they go to models, 
but that is not ipmlemented atm
*/
/*
I didnt implement any methods to modify or check user access.
No modify, because it would be basicly more boilerplate copy paste.
No user access, because i dont know how yet. 
I might return some time in the future to add them
*/

/*Events */

app.get("/api/events", (req, res) =>{

	/* not the best seperation, but it is functional */
	models.Event.find({/*all*/}).exec(dbCallback(res))
})

app.post("/api/events", (req, res) =>{
	
	models.createEvent(req.body, dbCallback(res))
})

app.get("/api/events/byid/:_id", (req, res) =>{
	//res.send(req.params._id)
	models.Event.findById(req.params._id).exec(dbCallback(res))
})

app.delete("/api/events/byid/:_id", (req, res) =>{
	//res.send(req.params._id)
	models.removeEvent(req.params._id, dbCallback(res))
})

/*
https://stackoverflow.com/questions/15128849/using-multiple-parameters-in-url-in-express
*/
app.get("/api/events/byinterval/:stdate/:enddate", (req, res)=>{
	var start = new Date(req.params.stdate)
	var end = new Date(req.params.enddate)
	models.getEventsBetween(start, end, dbCallback(res))
})

app.get("/api/events/bydate/:stdate", (req, res) =>{
	var start = new Date(req.params.stdate)
	models.getEventByDate(start, dbCallback(res))
})

/*Eras */
app.get("/api/eras", (req, res) =>{

	/* not the best seperation, but it is functional */
	models.Era.find({/*all*/}).exec(dbCallback(res))
})

app.post("/api/eras", (req, res) =>{

	models.createEra(req.body, dbCallback(res))
})

app.get("/api/eras/byid/:_id", (req, res) =>{
	//res.send(req.params._id)
	models.Era.findById(req.params._id).exec(dbCallback(res))
})

app.delete("/api/eras/byid/:_id", (req, res) =>{
	//res.send(req.params._id)
	models.removeEra(req.params._id, dbCallback(res))
})

app.get("/api/eras/bycontained/:stdate/:enddate", (req, res)=>{
	var start = new Date(req.params.stdate)
	var end = new Date(req.params.enddate)
	models.getErasContainedBetween(start, end, dbCallback(res))
})

app.get("/api/eras/byinterval/:stdate/:enddate", (req, res)=>{
	var start = new Date(req.params.stdate)
	var end = new Date(req.params.enddate)
	models.getErasActiveBetween(start, end, dbCallback(res))
})

/*
https://stackoverflow.com/questions/22673218/default-route-in-express-js 
*/
app.get('*',function (req, res) {
	res.redirect('/');
});

/*app */
app.listen(port,()=>{
	console.log("server started on port: "+ port)
})
