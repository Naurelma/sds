
/* input some random data to db to start with */
/* needs to be terminated wit ctr-c for some reason*/

const models = require("./models")

/*same ones i used to test it with */
models.Event.insertMany([
	{
		"name":"Monday",
		"startDate":"2018-12-3",
		"description":"a random day from the calendar"
	},
	{
		"name":"Tuesday",
		"startDate":"2018-12-11",
		"description":"a random spot from the calendar"
	},
	{
		"name":"Wednesday",
		"startDate":"2018-12-19",
		"description":"a random day from the calendar"
	},
	{
		"name":"Sunday",
		"startDate":"2018-12-2",
		"description":"a semirandom day from my wall"
	},
	],
	(err)=>{
		if (err){
			console.log(err)
		}
	})

models.Era.insertMany([
	{
		"name":"year 2018",
		"startDate":"2018-1-1",
		"endDate":"2018-12-31",
		"description":"a strange time"
	},
	{
		"name":"a month",
		"startDate":"2017-12-1",
		"endDate":"2017-12-31",
		"description":"Christmas of 2017"
	},
	{
		"name":"Week 50",
		"startDate":"2018-12-10",
		"endDate":"2018-12-16",
		"description":"out of ideas now"
	},
	], 
	(err)=>{
		if (err){
			console.log(err)
		}
	})


console.log("done")
setTimeout(function() {process.exit()}, 1000);
